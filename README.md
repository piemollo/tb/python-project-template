# Python project template

## Note
The name of the project must be modified in the `pyproject.toml` and `setup.cfg` files.
Dependencies must also be mentioned in these files.
More metadata can by provided in the `pyproject.toml` file.

By default, the licence here is [CeCILL](http://www.cecill.info/).

## To build the project

The following command build the project in the locally Python Packager 
(PIP)
```bash
> pip install -e .
```
The project can then be called using 
```python
import pytemplate as pyt
```
or in any other usual manner.